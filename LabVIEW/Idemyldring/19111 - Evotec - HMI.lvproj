﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="19008000">
	<Item Name="My Computer" Type="My Computer">
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="FB030" Type="Folder"/>
		<Item Name="FBO20" Type="Folder">
			<Item Name="Layout - Bundle to picture ring.vi" Type="VI" URL="../Layout/Layout - Bundle to picture ring.vi"/>
			<Item Name="Layout - PopupDialog.vi" Type="VI" URL="../Layout/Layout - PopupDialog.vi"/>
			<Item Name="PictureRing.ctl" Type="VI" URL="../Layout/PictureRing.ctl"/>
		</Item>
		<Item Name="Testing" Type="Folder">
			<Item Name="Test hex to pict ring 2.vi" Type="VI" URL="../Testing/Test hex to pict ring 2.vi"/>
			<Item Name="Test hex to pict ring.vi" Type="VI" URL="../Testing/Test hex to pict ring.vi"/>
		</Item>
		<Item Name="Evotec LabVIEW HMI - HMI Target - Dialog - Object.vi" Type="VI" URL="../../MAIN/Evotec LabVIEW HMI/[11] Modules/HMI Target/Dialog/Evotec LabVIEW HMI - HMI Target - Dialog - Object.vi"/>
		<Item Name="Send CMD.vi" Type="VI" URL="../Layout/Send CMD.vi"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="System.Windows.Forms" Type="Document" URL="System.Windows.Forms">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
</Project>
